import { SecondLifeMessagingTarget, PhotonApp, SecondLifeProxy } from "..";

class SecondLifeDevice {
  id: string;
  name: string;
  lastSeen: any;
  target: SecondLifeMessagingTarget;

  constructor(id: string, name: string, lastSeen: any) {
    this.id = id;
    this.name = name;
    this.lastSeen = lastSeen;
    this.target = new SecondLifeMessagingTarget(this.id, PhotonApp.appId, SecondLifeProxy.socket);
  }
}

export default SecondLifeDevice;