import SecondLifeDevice from "./SecondLifeDevice";
import { MessageBroker } from "..";
import SecondLifeRegistry from "./SecondLifeRegistry";

enum ChatVolume {
  Normal = 0,
  Whisper = 1,
  Shout = 2,
  RegionSay = 3,
}

enum SensorType {
  AGENT = 0x1,
}

type SensorOptions = {
  name?: string;
  key?: string | "OWNER";
  type?: SensorType;
  range?: number;
  arc?: number | "TWO_PI";
};

class PhotonTool {
  public device: SecondLifeDevice;

  constructor(device: SecondLifeDevice) {
    this.device = device;
  }

  async say(channel: number, message: string) {
    await this.chat(ChatVolume.Normal, channel, message);
  }

  async shout(channel: number, message: string) {
    await this.chat(ChatVolume.Shout, channel, message);
  }

  async whisper(channel: number, message: string) {
    await this.chat(ChatVolume.Whisper, channel, message);
  }

  async regionSay(channel: number, message: string) {
    await this.chat(ChatVolume.RegionSay, channel, message);
  }

  async ownerSay(message: string) {
    await MessageBroker.defaultMessageBroker.invoke(
      this.device.target,
      "pt_Communication",
      "ownerSay",
      [message]
    );
  }

  async chat(volume: ChatVolume, channel: number, message: string) {
    await MessageBroker.defaultMessageBroker.invoke(
      this.device.target,
      "pt_Communication",
      "say",
      [volume, channel, message]
    );
  }

  async sensor(options: SensorOptions = {}) {
    const results = await MessageBroker.defaultMessageBroker.invoke(
      this.device.target,
      "pt_Sensor",
      "sensor",
      [
        options.name || "",
        options.key || "NULL_KEY",
        options.type || SensorType.AGENT,
        options.range || 96,
        options.arc || "TWO_PI",
      ]
    );

    return results;
  }

  async giveInventoryItem(inventoryItemName: string, recipientUuid: string) {
    await MessageBroker.defaultMessageBroker.invoke(
      this.device.target,
      "pt_Inventory",
      "giveInventory",
      [inventoryItemName, recipientUuid]
    );
  }

  invokeCommand(command: string) {
    MessageBroker.defaultMessageBroker.invoke(
      this.device.target,
      "pt_Legacy",
      "invokeCommand",
      [command]
    );
  }

  static async getPhotonTool(): Promise<PhotonTool> {
    const deviceList = await SecondLifeRegistry.getDevices();

    const matchingDevice = deviceList.find(
      (d) => d.name.indexOf("Photon Tool") > -1
    );

    if (!matchingDevice) {
      throw new Error("Photon Tool is not connected.");
    }

    return new PhotonTool(matchingDevice);
  }

  static async isPhotonToolConnected(): Promise<boolean> {
    const deviceList = await SecondLifeRegistry.getDevices();

    const matchingDevice = deviceList.find(
      (d) => d.name.indexOf("Photon Tool") > -1
    );

    if (!matchingDevice) {
      return false;
    }

    return true;
  }
}

export default PhotonTool;
