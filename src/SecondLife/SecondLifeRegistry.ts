import SecondLifeDevice from "./SecondLifeDevice";
import { MessageBroker } from "..";

class SecondLifeRegistry {
  static async getDevices(): Promise<SecondLifeDevice[]> {
    const returnedDevices = await MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_SecondLifeRegistry",
      "getDevices"
    );

    return returnedDevices.map(
      (device: any) =>
        new SecondLifeDevice(device.id, device.name, device.lastSeen)
    );
  }
}

export default SecondLifeRegistry;
