import { MessageBroker } from "..";

class SecondLifeIdentity {
  static getKey() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("key");
  }
}

export default SecondLifeIdentity;
