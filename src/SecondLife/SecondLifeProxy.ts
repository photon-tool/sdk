import * as io from "socket.io-client";

class SecondLifeProxy {
  static proxyUrl: string = "https://photon-proxy.herokuapp.com";
  static socket: any = io(SecondLifeProxy.proxyUrl);
}

export default SecondLifeProxy;
