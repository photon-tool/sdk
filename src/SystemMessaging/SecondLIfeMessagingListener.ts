import IMessageListener from "./IMessageListener";
import MessageBroker from "./MessageBroker";
import { EventEmitter } from "events";
import SecondLifeMessagingTarget from "./SecondLifeMessagingTarget";
import SecondLifeIdentity from "../SecondLife/SecondLifeIdentity";

/**
 * Listens for messages on the browser messaging system.
 */
class SecondLifeMessagingListener
  extends EventEmitter
  implements IMessageListener
{
  id: string = "secondlife";
  senderId: string;
  socket: any;

  constructor(senderId: string, socket: any) {
    super();
    this.senderId = senderId;
    this.socket = socket;
  }

  subscribe(broker: MessageBroker): void {
    this.socket.emit("ident", {
      id: this.senderId,
      owner: SecondLifeIdentity.getKey(),
    });
    this.socket.on("invoke", this.messageEventHandler.bind(this));
  }

  unsubscribe(broker: MessageBroker): void {
    this.socket.off("invoke", this.messageEventHandler.bind(this));
  }

  private messageEventHandler(data: any) {
    const source = new SecondLifeMessagingTarget(
      data.__sender || "unknown",
      this.senderId,
      this.socket
    );
    this.emit("message", { source, data });
  }
}

export default SecondLifeMessagingListener;
