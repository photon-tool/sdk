import IMessagingTarget from "./IMessagingTarget";
import IMessageListener from "./IMessageListener";
import IMessage from "./IMessage";
import VoidMessagingTarget from "./VoidMessagingTarget";
import { v4 } from "uuid";

/**
 * The core messaging backend to Photon OS.
 * Instances of MessageBroker exist on each running app, as well as on the OS. The MessageBroker
 * simulataneously receives and dispatches methods to registered services, as well as allows
 * you to invoke methods on services registered on other brokers.
 */
class MessageBroker {
  static defaultMessageBroker: MessageBroker;

  targets: IMessagingTarget[];
  services: any[];
  listeners: IMessageListener[];
  returnHandlers: any;

  constructor() {
    this.targets = [];
    this.services = [];
    this.listeners = [];
    this.returnHandlers = {};
  }

  static setDefault(defaultMessageBroker: MessageBroker) {
    MessageBroker.defaultMessageBroker = defaultMessageBroker;
  }

  // region: external communication methods

  /**
   * Registers a messaging target on the broker.
   * @param target The messaging target to register.
   */
  registerTarget(target: IMessagingTarget) {
    if (this.hasTarget(target.id)) {
      console.warn(`Attempted to register duplicate target: ${target.id}`);
      return;
    }

    this.targets.push(target);
  }

  /**
   * Removes a messaging target from the broker.
   * @param targetId The messaging target ID.
   */
  unregisterTarget(targetId: string) {
    this.targets = this.targets.filter((t) => t.id !== targetId);
  }

  /**
   * Safely fetches a messaging target for the target ID.
   * @description If a messaging target for the ID is not found, a VoidMessagingTarget will be created and returned. This allows you to safely use this method. To check if a target exists, use the hasTarget method.
   * @param targetId The target ID to fetch.
   */
  getTargetById(targetId: string): IMessagingTarget {
    const matchingTarget = this.targets.find((t) => t.id === targetId);
    return matchingTarget || new VoidMessagingTarget(targetId);
  }

  /**
   * Checks if the broker has the specified target.
   * @param targetId The target ID to check.
   */
  hasTarget(targetId: string): boolean {
    return this.targets.find((t) => t.id === targetId) !== undefined;
  }

  /**
   * Invoke a service method on the target.
   * @param target The target to invoke the service method on.
   * @param serviceId The service to access.
   * @param methodName The method on the service to invoke.
   * @param methodArgs Arguments for the service method.
   */
  invoke(
    target: IMessagingTarget,
    serviceId: string,
    methodName: string,
    methodArgs?: any[]
  ): Promise<any> {
    // console.log(`Invoke ${serviceId}.${methodName} on ${target.id}`);

    return new Promise((resolve) => {
      const returnId = `${methodName}_${v4()}`;
      this.registerReturnHandler(returnId, resolve);

      target.postMessage({
        __type: "invoke",
        serviceId,
        methodName,
        methodArgs,
        returnId,
      });
    });
  }

  /**
   * Invoke a service method on all registered targets.
   * @param serviceId The service to access.
   * @param methodName The method on the service to invoke.
   * @param methodArgs Arguments for the service method.
   */
  invokeEverywhere(serviceId: string, methodName: string, methodArgs?: any[]) {
    this.targets.forEach((target) =>
      this.invoke(target, serviceId, methodName, methodArgs)
    );
  }

  private registerReturnHandler(returnId: string, callback: Function) {
    this.returnHandlers[returnId] = callback;
  }

  private unregisterReturnHandler(returnId: string) {
    delete this.returnHandlers[returnId];
  }

  private handleReturn(message: IMessage) {
    const { returnId, returnValue } = message.data;
    const returnHandler = this.returnHandlers[returnId];

    if (returnHandler === undefined) {
      console.warn(`Attempted to invoke unknown return handler: ${returnId}`);
      return;
    }

    returnHandler(returnValue);
    this.unregisterReturnHandler(returnId);
  }

  // endregion

  // region: internal methods

  /**
   * Registers a service on the broker, allowing other brokers to use it.
   * @param serviceId The service ID.
   * @param service The service implementation.
   */
  registerService(serviceId: string, service: any) {
    const existingService = this.getServiceById(serviceId);

    if (existingService !== undefined) {
      console.warn(`Attempted to regsiter duplciate service: ${serviceId}`);
      return;
    }

    this.services.push({ id: serviceId, methods: service });
  }

  /**
   * Removes a service from the broker.
   * @param serviceId The service ID.
   */
  unregisterService(serviceId: string) {
    this.services = this.services.filter((s) => s.id !== serviceId);
  }

  /**
   * Fetches a service from the list of registered services.
   * @param serviceId The service ID.
   */
  getServiceById(serviceId: string) {
    return this.services.find((s) => s.id === serviceId);
  }

  /**
   * Registers and subcribes a listener on the broker.
   * @param listener The listener to register.
   */
  registerListener(listener: IMessageListener) {
    const existingListener = this.getListenerByid(listener.id);

    if (existingListener !== undefined) {
      console.warn(`Attempted to register duplicate listener: ${listener.id}`);
      return;
    }

    this.listeners.push(listener);
    listener.on("message", this.handleMessageEvent.bind(this));
    listener.subscribe(this);
  }

  /**
   * Unsubscribes and unregisters a listener from the broker.
   * @param listenerId The listener ID to remove.
   */
  unregisterListener(listenerId: string) {
    const existingListener = this.getListenerByid(listenerId);
    this.listeners = this.listeners.filter((l) => l.id !== listenerId);

    if (existingListener !== undefined) {
      existingListener.off("message", this.handleMessageEvent.bind(this));
      existingListener.unsubscribe(this);
    }
  }

  /**
   * Fetches a listener from the broker.
   * @param listenerId The listener ID to fetch.
   */
  getListenerByid(listenerId: string) {
    return this.listeners.find((l) => l.id === listenerId);
  }

  // endregion

  // region: message handling

  private handleMessageEvent(message: IMessage) {
    if (message.data.__type === undefined) return; // no-op

    switch (message.data.__type) {
      case "invoke":
        this.handleInvoke(message);
        break;
      case "return":
        this.handleReturn(message);
        break;
      default:
        return; // TODO: Log error about invalid message type
    }
  }

  private async handleInvoke(message: IMessage) {
    const { serviceId, methodName, methodArgs, returnId } = message.data;
    const matchingService = this.getServiceById(serviceId);

    if (matchingService === undefined) {
      console.error(`Attempted to invoke unknown service: ${serviceId}`);
      return;
    }

    const matchingMethod = matchingService.methods[methodName];

    if (matchingMethod === undefined) {
      console.error(
        `Attepted to invoke unknown method: ${serviceId}.${methodName}`
      );
      return;
    }

    const returnValue = await matchingMethod(...(methodArgs || []));
    this.sendReturnValue(message.source, returnId, returnValue);
  }

  private sendReturnValue(
    target: IMessagingTarget,
    returnId: string,
    returnValue: any
  ) {
    target.postMessage({
      __type: "return",
      returnId,
      returnValue,
    });
  }
}

export default MessageBroker;
