import IMessagingTarget from "./IMessagingTarget";

/**
 * A placeholder messaging target that does nothing with the
 * messages posted to it.
 */
class VoidMessagingTarget implements IMessagingTarget {
  id: string;
  
  constructor(id: string) {
    this.id = id;
  }
  
  postMessage(message: any): void {
    console.warn(`Message posted to void target: ${this.id}`, message);
    return;
  }
}

export default VoidMessagingTarget;