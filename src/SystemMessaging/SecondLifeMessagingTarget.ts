import IMessagingTarget from "./IMessagingTarget";

class SecondLifeMessagingTarget implements IMessagingTarget {
  id: string;
  senderId: string;
  socket: any;

  constructor(targetId: string, senderId: string, socket: any) {
    this.id = targetId;
    this.senderId = senderId;
    this.socket = socket;
  }
  
  postMessage(message: any): void {
    this.socket.emit('invoke', {
      __recipient: this.id,
      __sender: this.senderId,
      ...message
    });
  }
}

export default SecondLifeMessagingTarget;