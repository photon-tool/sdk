import IMessagingTarget from './IMessagingTarget';

/**
 * Allows sending messages via the Window.postMessage API.
 */
class BrowserMessagingTarget implements IMessagingTarget {
  id: string;
  target: Window;
  origin: string;
  
  constructor(id: string, target: Window, origin: string = '*') {
    this.id = id;
    this.target = target;
    this.origin = origin;
  }

  postMessage(message: any): void {
    this.target.postMessage(message, this.origin);
  }
}

export default BrowserMessagingTarget;