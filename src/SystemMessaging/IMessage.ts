import IMessagingTarget from "./IMessagingTarget";

/**
 * Represents a message sent across the message brokering system.
 */
interface IMessage {
  source: IMessagingTarget,
  data: any
}

export default IMessage;