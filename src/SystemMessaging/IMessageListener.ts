import MessageBroker from "./MessageBroker";
import { EventEmitter } from "events";

/**
 * Defines an interface to allow the local client to listen
 * to various types of messages from various sources.
 */
interface IMessageListener extends EventEmitter {
  id: string;

  /**
   * Subscribes the listener.
   * @param broker The broker this listener is registered to.
   */
  subscribe(broker: MessageBroker): void;

  /**
   * Unsubscribes the listener.
   * @param broker The broker this listener is registered to.
   */
  unsubscribe(broker: MessageBroker): void;
}

export default IMessageListener;