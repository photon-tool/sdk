import IMessageListener from "./IMessageListener";
import MessageBroker from "./MessageBroker";
import { EventEmitter } from "events";
import BrowserMessagingTarget from "./BrowserMessagingTarget";
import { v4 } from "uuid";

/**
 * Listens for messages on the browser messaging system.
 */
class BrowserMessageListener extends EventEmitter implements IMessageListener {
  id: string = "browser";

  subscribe(broker: MessageBroker): void {
    window.addEventListener("message", this.messageEventHandler.bind(this));
  }

  unsubscribe(broker: MessageBroker): void {
    window.removeEventListener("message", this.messageEventHandler.bind(this));
  }

  private messageEventHandler(e: MessageEvent) {
    const source = new BrowserMessagingTarget("randomid", e.source as Window);
    this.emit("message", { source, data: e.data });
  }
}

export default BrowserMessageListener;
