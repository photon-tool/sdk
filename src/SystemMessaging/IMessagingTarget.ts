/**
 * Defines an interface to locally represent a messaging target
 * across varying diffeent platform types.
 */
interface IMessagingTarget {
  id: string;

  /**
   * Send a message to the target.
   * @param message The message to send.
   */
  postMessage(message: any): void;
}

export default IMessagingTarget;