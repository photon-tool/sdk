import { EventEmitter } from "events";
import { EventManager } from "..";
import LifecycleEventNames from "./LifecycleEventNames";

/**
 * Provides access to the appliation lifecycle events for your app.
 */
class LifecycleEvents extends EventEmitter {
  /**
   * Subscribes to a lifecycle event.
   * @param eventName The lifecycle event to subscribe to.
   * @param handler The handler to invoke when the event is triggered.
   */
  static on(eventName: LifecycleEventNames, handler: Function) {
    switch (eventName) {
      case 'willEnterBackground':
        EventManager.defaultEventManager.on('os_Launcher_WillEnterBackground', handler);  
        break;
      case 'willEnterForeground':
        EventManager.defaultEventManager.on('os_Launcher_WillEnterForeground', handler);
        break;
      default:
        throw new Error('Invalid Lifecycle Event: ' + eventName);
    }
  }
}

export default LifecycleEvents;