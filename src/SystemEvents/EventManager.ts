import MessageBroker from "../SystemMessaging/MessageBroker";
import IMessagingTarget from "../SystemMessaging/IMessagingTarget";

/**
 * Provides the backbone of the events system. Each messaging target should
 * use an event manager to allow the target to send and receive events.
 * Initializing the EventManager registers an EventManager service locally,
 * which is invoked from other targets to send events to this target.
 */
class EventManager {
  static defaultEventManager: EventManager;

  handlers: {
    [eventName: string]: Function[]
  } = {};

  constructor() {
    MessageBroker.defaultMessageBroker.registerService('os_EventManager', {
      handleEvent: (event: { eventName: string, eventArgs?: any }) => {
        // console.log(`Event Incoming: `, event.eventName);
        if (!this.handlers[event.eventName]) return;
        this.handlers[event.eventName].forEach((h: Function) => h(event.eventArgs));
      }
    });
  }

  /**
   * Subscribes to an event.
   * @param eventName The event name
   * @param handler The handler to invoke
   */
  on(eventName: string, handler: Function) {
    this.handlers[eventName] = this.handlers[eventName] || [];
    this.handlers[eventName].push(handler);
  }

  /**
   * Unsubscribes from an event.
   * @param eventName The event name
   * @param handler The handler to unsubscribe. If not specified, all handlers will be removed.
   */
  off(eventName: string, handler?: Function) {
    if (handler) {
      this.handlers[eventName] = this.handlers[eventName] || [];
      this.handlers[eventName] = this.handlers[eventName].filter(h => h !== handler);
    } else {
      delete this.handlers[eventName];
    }
  }

  /**
   * Emits an event to all registered targets.
   * @param eventName The event name to emit.
   * @param eventArgs Arguments to send with the event.
   */
  emit(eventName: string, eventArgs?: any) {
    MessageBroker.defaultMessageBroker.invokeEverywhere(
      'os_EventManager',
      'handleEvent',
      [{ eventName, eventArgs }]
    );
  }

  /**
   * Emits an event to a specific target.
   * @param target The target to emit the event to.
   * @param eventName The event name to emit.
   * @param eventArgs Arguments to send with the event.
   */
  emitTo(target: IMessagingTarget, eventName: string, eventArgs?: any) {
    MessageBroker.defaultMessageBroker.invoke(
      target,
      'os_EventManager',
      'handleEvent',
      [{ eventName, eventArgs }]
    );
  }

  /**
   * Sets the default event manager, allowing it to be accessed easily elsewhere in the code.
   * @param eventManager The event manager to use as the default event manager.
   */
  static setDefault(eventManager: EventManager) {
    EventManager.defaultEventManager = eventManager;
  }
}

export default EventManager;