/**
 * Application Lifecycle Events
 */
enum LifecycleEventNames {
  /**
   * Invoked when the application is launched, or enters the foreground.
   */
  WillEnterForeground = 'willEnterForeground',
  
  /**
   * Invoked when the application is terminated, or enters the background.
   */
  WillEnterBackground = 'willEnterBackground'
}

export default LifecycleEventNames;