/**
 * Determines what type of app is running.
 * The default is typically "App", but the operating system runs with the "Host" app type.
 */
enum PhotonAppMode {
  Host,
  App
}

export default PhotonAppMode;