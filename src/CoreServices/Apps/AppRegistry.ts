import MessageBroker from '../../SystemMessaging/MessageBroker';

class AppRegistry {
  static installApp(id: string, name: string, url: string) {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById('os'),
      'os_AppRegistry',
      'installApp',
      [id, name, url]
    );
  }

  static uninstallApp(id: string) {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById('os'),
      'os_AppRegistry',
      'uninstallApp',
      [id]
    );
  }

  static getInstalledApps(): Promise<any[]> {
    return MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById('os'),
      'os_AppRegistry',
      'getInstalledApps'
    );
  }
}

export default AppRegistry;