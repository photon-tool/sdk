import EventManager from "../../SystemEvents/EventManager";
import {
  BrowserMessagingTarget,
  MessageBroker,
  SecondLifeMessagingListener,
} from "../..";
import BrowserMessageListener from "../../SystemMessaging/BrowserMessageListener";
import PhotonAppMode from "./PhotonAppMode";
import SecondLifeProxy from "../../SecondLife/SecondLifeProxy";
import SecondLifeIdentity from "../../SecondLife/SecondLifeIdentity";

/**
 * Contains a variety of helpful methods for initializing your Photon OS App.
 */
class PhotonApp {
  static appId: string;

  /**
   * Initializes a message broker and event manager for your app, and assigns them as the default.
   * @param appMode The mode to bootstrap your app in.
   */
  static async bootstrap(
    appId: string,
    appMode: PhotonAppMode = PhotonAppMode.App
  ) {
    PhotonApp.appId = appId;

    const messageBroker = new MessageBroker();
    MessageBroker.setDefault(messageBroker);

    messageBroker.registerTarget(
      new BrowserMessagingTarget(
        "os",
        appMode === PhotonAppMode.App ? window.parent : window
      )
    );

    const key = SecondLifeIdentity.getKey();

    messageBroker.registerListener(
      new SecondLifeMessagingListener(
        `${appId}__${key}`,
        SecondLifeProxy.socket
      )
    );

    messageBroker.registerListener(new BrowserMessageListener());

    const eventManager = new EventManager();
    EventManager.setDefault(eventManager);
  }
}

export default PhotonApp;
