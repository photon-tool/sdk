import App from "./App";
import { MessageBroker } from '../../';

/**
 * Manages app launching on the operating system.
 */
class Launcher {
  /**
   * Launches or resumes the specified app.
   * @param app The app to launch or resume.
   */
  static launch(app: App) {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById('os'),
      'os_Launcher',
      'launchApp',
      [app]
    )
  }
}

export default Launcher;