import MessageBroker from "../../SystemMessaging/MessageBroker";

class UserPreferences {
  static setItem(key: string, value: string) {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_UserPreferences",
      "setItem",
      [key, value]
    );
  }

  static getItem(key: string) {
    return MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_UserPreferences",
      "getItem",
      [key]
    );
  }

  static removeItem(key: string) {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_UserPreferences",
      "removeItem",
      [key]
    );
  }
}

export default UserPreferences;
