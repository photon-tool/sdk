import MessageBroker from "../../SystemMessaging/MessageBroker";
import StatusBarContentType from "./StatusBarContentType";
import { EventEmitter } from "events";

/**
 * Controls the Status Bar
 */
class StatusBar {
  /**
   * Sets the status bar content type.
   * @param contentType The type of content the status bar should adapt to.
   */
  static async setContentType(contentType: StatusBarContentType): Promise<any> {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_StatusBar",
      "setContentType",
      [contentType === StatusBarContentType.LightContent ? "light" : "dark"]
    );
  }

  /**
   * Returns the content type the status bar is currently adapted to.
   */
  static async getContentType(): Promise<StatusBarContentType> {
    return MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_StatusBar",
      "getContentType"
    );
  }

  /**
   * Sets the status bar content type.
   * @param contentType The type of content the status bar should adapt to.
   */
  static async setHidden(hidden: boolean): Promise<any> {
    MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_StatusBar",
      "setHidden",
      [hidden]
    );
  }

  /**
   * Returns the content type the status bar is currently adapted to.
   */
  static async getHidden(): Promise<boolean> {
    return MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_StatusBar",
      "getHidden"
    );
  }
}

export default StatusBar;
