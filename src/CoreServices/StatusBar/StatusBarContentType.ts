enum StatusBarContentType {
  /**
   * Adapts the status bar to light content, such as bright images
   * or screens with a white background.
   */
  LightContent,
  
  /**
   * Adapts the status bar to dark content, such as dark images,
   * or screens with a dark background.
   */
  DarkContent
}

export default StatusBarContentType;