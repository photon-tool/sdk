type SensorResult = {
  id: string;
  key: string;
  name: string;
  owner: string;
  position: { x: number; y: number; z: number };
  rotation: number[];
  type: number;
  lastSeen: Date;
};
