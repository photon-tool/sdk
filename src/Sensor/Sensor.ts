import SecondLifeDevice from "../SecondLife/SecondLifeDevice";
import { MessageBroker, EventManager } from "..";
import SecondLifeRegistry from "../SecondLife/SecondLifeRegistry";

class Sensor {
  static async getData(): Promise<SensorResult[]> {
    const sensorData = await MessageBroker.defaultMessageBroker.invoke(
      MessageBroker.defaultMessageBroker.getTargetById("os"),
      "os_Sensor",
      "getData"
    );

    return sensorData;
  }

  static on(eventName: "change", handler: Function) {
    if (eventName !== "change") return;
    EventManager.defaultEventManager.on("os_Sensor_Changed", handler);
  }

  static off(eventName: "change", handler?: Function) {
    if (eventName !== "change") return;
    EventManager.defaultEventManager.off("os_Sensor_Changed", handler);
  }
}

export default Sensor;
